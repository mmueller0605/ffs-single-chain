/* Copyright (C) 2020 Ludwig Schneider, Marcus Mueller

   This is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License.
   If not, see <http://www.gnu.org/licenses/>.

   The code for the PCG random number generation is derived work from
   the original PCG software "http://www.pcg-random.org/" the license
   is Apache version 2. A license text is found in the file
   Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
   "PCG_LICENSE.txt"
*/

/*----------------------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <time.h>
#include <stdint.h>
#include "pcg.h"

/*----------------------------------------------------------------------------------------------*/
//! \brief State of the random number generator (PCG)
typedef struct PCG_STATE {
        uint64_t        state;  //!<\brief internal state of the PCG generator
        uint64_t        inc;    //!<\brief stream of the PCG generator
} PCG_STATE;

/*----------------------------------------------------------------------------------------------*/
//! Function to seed the random number generator.
//!
//! \param stream Stream of the RNG.
//! \param rng PCG_STATE to seed
//! \param seed Seed for the rng.
//! \return Error code.
static int     seed_rng(PCG_STATE * rng, uint64_t seed, uint64_t stream);

//*! Random number generator PCG32
//*! \param rng State for PRNG
//*! \return PRN
static inline uint32_t pcg32_random(PCG_STATE * rng);

//! Return a scalar random number in [0,1)
//! \param rng state of a PRNG
//! \return PRN in [0,1)
static inline double  pcg_random_scalar(PCG_STATE * rng);

/*----------------------------------------------------------------------------------------------*/
/* global variable */
static PCG_STATE	*parallel_states;		/* one for each thread */
static double		*store;				/* additional result for Box-Mueller */
static int		*empty;				/* additional results not available */

/*----------------------------------------------------------------------------------------------*/
void 	init_pcg32(void)
{
 	const int nthreads = omp_get_num_procs();	/* initialze OpenMP */
        omp_set_num_threads(nthreads);
        fprintf(stderr,"INFO: init_pcg32 -- omp with %d threads\n",nthreads);
	const uint64_t seed = time(NULL);		/* initialize RND */
        fprintf(stderr,"INFO: init_pcg32 -- seed= %llu\n",(long long unsigned)seed);
	parallel_states = (PCG_STATE*)calloc(nthreads,sizeof(PCG_STATE));
	store = (double*)calloc(nthreads,sizeof(double));
	empty = (int*)calloc(nthreads,sizeof(int));
        if ((parallel_states==NULL)||(store==NULL)||(empty==NULL)) {
                fprintf(stderr,"ERROR: Cannot allocate memory\n");
                exit(-1);
        }
        for (int ithread=0;ithread<nthreads;ithread++) {
                seed_rng(&(parallel_states[ithread]),seed,ithread);
	  	empty[ithread] = 1;
	}
}/* end init_pcg32 */

/*----------------------------------------------------------------------------------------------*/
void	end_pcg32(void)
{
  	free(empty);
	free(store);
  	free(parallel_states);
        fprintf(stderr,"INFO: end_pcg32 -- free states\n");
}/* end end_pcg32 */

/*----------------------------------------------------------------------------------------------*/
unsigned int	int_pcg32(void)
{
	const int ithread = omp_get_thread_num();
	unsigned int rnd  = (unsigned int)pcg32_random(parallel_states+ithread);
  	return(rnd);
}/* end int_pcg32 */

/*----------------------------------------------------------------------------------------------*/
double	double_pcg32(void)
{
	const int ithread = omp_get_thread_num();
  	return(pcg_random_scalar(parallel_states+ithread));
}/* end int_pcg32 */

/*----------------------------------------------------------------------------------------------*/
double 	gauss_pcg32(void)
{
	const int ithread = omp_get_thread_num();
	if (empty[ithread]) {
double		u1,u2,r,root;
		do {
			u1 = 2.*double_pcg32()-1.;
			u2 = 2.*double_pcg32()-1.;
			r  = u1*u1 + u2*u2;
		} while (r>1.0);
		root = sqrt(-2.0*log(r)/r);
		store[ithread] = u2*root;
		empty[ithread] = 0;
		return(u1*root);
	}
    	else {
		empty[ithread] = 1;
		return(store[ithread]);
    	}
}/* end gauss_pcg32 */

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
static int seed_rng(PCG_STATE * rng, uint64_t seed, uint64_t stream)
{
	rng->inc    = stream * 2 + 1;
    	rng->state  = 0;
    	pcg32_random(rng);
    	rng->state += seed;
    	pcg32_random(rng);
    	pcg32_random(rng);	/* improve quality of first random numbers */
    	return(0);
}/* end seed_rng */

/*----------------------------------------------------------------------------------------------*/
static inline uint32_t pcg32_random(PCG_STATE * rng)
{
	const uint64_t old = rng->state;
    	rng->state = ((uint64_t) rng->state) * 0X5851F42D4C957F2DULL;	/* advance internal state */
    	rng->state += (rng->inc | 1);
    	const uint32_t xorshifted = ((old >> 18u) ^ old) >> 27u;
    	const uint32_t rot = old >> 59u;
    	return((xorshifted >> rot) | (xorshifted << ((-rot) & 31)));
}/* pcg32_random */

/*----------------------------------------------------------------------------------------------*/
static inline double pcg_random_scalar(PCG_STATE * rng)
{
    return(ldexp(pcg32_random(rng), -32));
}/* end pcg_random_scalar */

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
