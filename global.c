/*----------------------------------------------------------------------------------------------*/
#include <stddef.h>

/*----------------------------------------------------------------------------------------------*/
/* global variables */


double	eps   	   = 0;		/* interaction parameter */
double  invzc 	   = 0;		/* inverse of number of interaction partners, intra- and intermolecular */
double	L     	   = 10;	/* system size in units of Re */
double	*xcoord    = NULL;	/* coordinates of all monomers */
unsigned int 	n  = 0;		/* number of polymers */
unsigned int 	N  = 0;		/* number of monomers per polymer */
unsigned int    NA = 0;

int     reverse    =  0;      	/* reverse=0: L->R reverse=1: R->L */
double  CV_FF      =  0;
int     task       = -1;
int	iCV        =  0;
int	irun       =  0;

/* depends on L, see above */
double  CV_border  = 5; 	/* order parameter that marks border between basins */
double  CV_start   = 2.82; 	/* order parameter that marks the starting basin; reset in poly.c */
double  CV_end     = 7.18;	/* order parameter that marks the target basin */
double  CVR_start  = 7.18;	/* order parameter that marks the starting basin */
double  CVR_end    = 2.82;	/* order parameter that marks the target basin */

double  (*calc_CV)(unsigned int ipoly);

/*----------------------------------------------------------------------------------------------*/
