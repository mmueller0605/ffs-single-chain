#!/bin/bash

FLAGS="-bli4 -npcs -sob -br -nce -nut "

#HEADER files
for file in *.h
do
    indent $FLAGS $file
done

#SOURCE files
for file in *.c
do
    indent $FLAGS $file
done
