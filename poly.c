#define TEST
/*----------------------------------------------------------------------------------------------*/
/*
 * mpicc -lm -O3 -o P -Wall -fopenmp poly.c  global.c poly_lib.c pcg.c
 * /opt/local/bin/mpicc-mpich-gcc12 -lm -O3 -o P -x c -std=gnu99 -Wall -pedantic -fopenmp  poly.c global.c poly_lib.c pcg.c
 *
 * usage P CV n N Nsteps [task, CV_FF, iCV irun]
 *
 * CV
 *  0		center-of-mass
 *  1		junction point
 *
 * task		action
 * -1		MC simulation
 *  0		MC and dump configurations inside basin (ie not in opposite basin, CV < CV_end)
 *  1		escape from basin
 *  2		reach FF interface at CV_FF
 */
/*----------------------------------------------------------------------------------------------*/
#include "poly_lib.h"
#ifndef M_PI
#define M_PI           3.14159265358979323846  /* pi */
#endif

/*----------------------------------------------------------------------------------------------*/
int     readconf(int *ptime);
int	breaksym(double *coord,const unsigned int ipoly);
int     writeconf(int *ptime,char *fname);
void    ana(int istep,double *x0);
void    (*mc)   (const int istep,int *ptime,unsigned long *try_adr,unsigned long *acc_adr,double *dEacc_adr);
void    mc_local(const int istep,int *ptime,unsigned long *try_adr,unsigned long *acc_adr,double *dEacc_adr);
int     dump(const int ipoly,const int istep,int *ptime);
void    save(int *ptime,int dt);
int     calc_active(int *ptime);

/*----------------------------------------------------------------------------------------------*/
int main(int argc,char **argv)
{
char    command_line[2048]="#";
        for (int i=0;i<argc;i++) {
                strncat(command_line," ",2047);
                strncat(command_line,argv[i],2047);
        }
        strncat(command_line,"\n",2047);
        fprintf(stderr,"INFO: %s",command_line);
/* read in parameters from command line */
int	i = 1;
int	RC    	= atoi(argv[i++]);			/* 0 or 1 */
	n   	= atoi(argv[i++]);			/* global variable */
	N      	= atoi(argv[i++]);
	NA      = N/2;					/* NA=0 B-homopolymer -- NA=N/2 symmetric diblock -- NA=N B-homopolymer */
	if ((NA<0)||(NA>N)) {
	  	fprintf(stderr,"ERROR: NA= %u   N= %u˜n",NA,N);
		exit(-1);
	}
	eps     = 0.02;
	invzc   = 0.02;
	fprintf(stderr,"INFO: n= %u   N= %u (%u)   eps= %g   invzc= %g\n",n,N,NA,eps,invzc);
int	Nsteps 	= atoi(argv[i++]);
	if (i!=argc)
		task  = atoi(argv[i++]);
	if (i!=argc)
		CV_FF = atof(argv[i++]);
	if (i!=argc) {
	  	iCV   = atoi(argv[i++]);
	  	irun  = atoi(argv[i++]);
	}

	mc = mc_local;					/* choose MC move */
	if (RC==0) {
		calc_CV  = calc_xc;			/* choose collective variable, xc */
		CV_start = 2.82;			/* definition of basin */
	}
	else if (RC==1) {
		calc_CV  = calc_xj;			/* choose collective variable, xj */
		CV_start = 2.82;
	}
	else if (RC==2) {
	  	calc_CV  = calc_xcB;			/* choose collective variable, xcB */
		CV_start = 2.42;
	}
	else {
	  	fprintf(stderr,"ERROR: RC= %d != 0 (xc) or 1 (jp) or 2 (xcB) \n",RC);
		exit(-1);
	}

	if ((CV_FF<0)||(Nsteps<0)) {
	  	reverse = 1;
		fprintf(stderr,"INFO: reverse -- all CV values are negative\n");
		Nsteps = abs(Nsteps);
		CV_FF  = -fabs(CV_FF);
	}
	else {
	  	reverse = 0;
		fprintf(stderr,"INFO: normal -- all CV values are positive\n");
	}

/* initialze OpenMP */
int	n_threads = omp_get_num_procs();
	omp_set_num_threads(n_threads);
	fprintf(stderr,"INFO: omp with %d threads\n",n_threads);
/* initialize RND generator */
	init_pcg32();					/* set up random number generator */

/* allocate memory */
	xcoord = (double*)calloc(6*n*N,sizeof(double));
	if (xcoord==NULL) {
	  	fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
double*	x0 = xcoord + 3*n*N;
int*	ptime = (int*)calloc(5*n,sizeof(double));	/* 0: ptime, 1: mscore, 2: ipoly in input file, 3: time at read, 4: time since escape */
	if (ptime==NULL) {
	  	fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
/* initialize: read in omega fields and starting configuration */
	calc_Enb_extf(0,0);				/* initialize omega fields and read L */
	readconf(ptime);
	for (unsigned int i=0;i<3*n*N;i++)
	  	xcoord[i] = x0[i];			/* copy starting configuration to configuration */
/* set up task and sanity checks */
	switch(task) {
	  	case -1:
		  	fprintf(stderr,"INFO: MC simulation (do not dump basin)\n");
			break;
	  	case 0:
		  	fprintf(stderr,"INFO: MC simulation (and dump basin) CV_FF marks border between basins\n");
			for (int ipoly=0;ipoly<n;ipoly++) {
	  			ptime[ipoly]   = 0;
				ptime[ipoly+n] = calc_m(ipoly);
			}
			if (fabs(CV_FF-CV_border)>1e-6) {
			  	fprintf(stderr,"WARNING: task= %d (inside basin) CV_FF = %g != %g = CV_border -> fixed\n",
				    	task,CV_FF,CV_border);
				CV_FF = CV_border;
			}
			break;
		case 1:
		  	fprintf(stderr,"INFO: MC escape from basin CV_FF marks opposite basin\n");
			if (fabs(CV_FF-CV_end)>1e-6) {
			  	fprintf(stderr,"ERROR: task= %d (escape basin) but CV_FF= %g != %g = CV_end (right, final) border of basin -> fixed\n",
				    	task,CV_FF,CV_end);
				CV_FF = CV_end;
			}
			for (int ipoly=0;ipoly<n;ipoly++) {
	  			ptime[ipoly]   = 0;		/* when/how stopped */
				ptime[ipoly+n] = calc_m(ipoly);	/* previous mscore */
				if (ptime[ipoly+n]==2) {
					fprintf(stderr,"ERROR: polymer %u ; m= %d != 0,1 (opposite basin) CV= %g\n",
					    	ipoly,calc_m(ipoly),calc_CV(ipoly));
					exit(-1);
				}
			}
			break;
		case 2: {
		  	fprintf(stderr,"INFO: MC reach interface\n");
double			CVmin = +1000;
double			CVmax = -1000;
			for (int ipoly=0;ipoly<n;ipoly++) {
double				CV = calc_CV(ipoly);
				if (CV<CVmin)
				  	CVmin = CV;
				if (CV>CVmax)
				  	CVmax = CV;
			}/* ipoly */
#ifndef CHECKM
double			scale = 50;
int			i     = (int)floor(CVmax*scale)+1;
		  	CV_FF = (double)i/scale;
			fprintf(stderr,"INFO: set CV_FF= %g\n",CV_FF);
FILE*			fp = fopen("CV.dat","a");
			if (fp==NULL) {
			  	fprintf(stderr,"ERROR: Cannot open CV.dat\n");
				exit(-1);
			}
			else {
			  	fprintf(fp,"%g\n",CV_FF);
			  	fclose(fp);
			}
#endif
			for (unsigned int ipoly=0;ipoly<n;ipoly++) {
int				m  = calc_m(ipoly);
double				CV = calc_CV(ipoly);
/*				fprintf(stderr,"INFO: polymer %u m= %d   CV= %g\n",ipoly,m,CV);*/
				if (m!=1) {
				  	fprintf(stderr,"WARNING: polymer %d already has reached m= %d (0= basin, 2=target) CV= %g\n",ipoly,m,CV);
					/* rounding error due to io; should test previous interface */
				}
	  			ptime[ipoly]   = 0;
				ptime[ipoly+n] = m;
			}
		  	fprintf(stderr,"INFO: all polymers have m= 1 (or 2)   CVmin= %g   CVmax= %g\n",CVmin,CVmax);
			break;}
		default:
			fprintf(stderr,"ERROR: task= %d unknown\n",task);
			exit(-1);
			break;
	}
	ana(0,x0);
/* perform simulation */
unsigned long 	ntry   = 0;
unsigned long	acc    = 0;
int	istep  = 0;
int	active = n;
double	Estart = calc_E();
double	dEacc  = 0;
	for (;((task<=1)&&(istep<Nsteps))||((task==2)&&(active>0)&&(istep<1000000));) {
		istep++;
	 	mc(istep,ptime,&ntry,&acc,&dEacc);
/*		save(ptime,10);	*/
		if (istep%10==0) {				/* analysis every (used to be) 1000 steps */
			ana(istep,x0);
		}
		if (istep%1000==0) {
		  	active = calc_active(ptime);
		  	fprintf(stderr,"INFO: %d active polymers at istep %d\n",active,istep);
		}
	}
/* check that energy is correctly counted during MC simulation */
double	Eend = calc_E();
	if ((fabs(Eend-Estart-dEacc)>n*N*1e-9)&&(task!=1)) {	/* exclude task=1 (escape) because replacing chain that reaches opposite basin breaks energy count */
	  	fprintf(stderr,"ERROR: energy Estart= %g + dEacc= %g = Eend= %g != %g   diff= %g\n",Estart,dEacc,Estart+dEacc,Eend,Eend-Estart-dEacc);
		exit(1);
	}
	fprintf(stderr,"INFO: MC acc= %g = %lu / %lu\n",(double)acc/(double)ntry,acc,ntry);
/*write out results and configuration */
	ana(-istep,x0);						/* write out results */
	dump(-1,istep,ptime); 					/* write out time.dat */
	writeconf(ptime,NULL);					/* write conf.dat */
/* if appropriate, check that no polymer remains active, ie has not fallen back to starting basin or has not reached FF interface */
  	active = calc_active(ptime);
	if ((task<2)||(active==0))
		return(0);
	else {
	  	fprintf(stderr,"ERROR: task= %d but active= %d > 0 at istep= %d\n",task,active,istep);
	  	exit(-1);
	}
	free(ptime);
	free(xcoord);
	end_pcg32();
}/* end main */

/*----------------------------------------------------------------------------------------------*/
int	genconf(double *coord) 	/* generate initial configuration of Gaussian chains */
{
double  b  = 1.0/sqrt(3.*(N-1));
double	R2 = 0;

	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
int	  	index = 3*N*ipoly;
	  	coord[index] = L*double_pcg32();
		index++;
	  	coord[index] = 0;
		index++;
	  	coord[index] = 0;
		index++;
		for (unsigned int imono=1;imono<N;imono++) {
		  	coord[index] = coord[index-3] + b* gauss_pcg32();	/* x-coordinate */
			index++;
		  	coord[index] = coord[index-3] + b* gauss_pcg32();	/* y-coordinate */
			index++;
		  	coord[index] = coord[index-3] + b* gauss_pcg32();	/* z-coordinate */
			index++;
		}
		if ((NA!=0)&&(NA!=N)) {
double			xj  = coord[3*(ipoly*N+NA-1)];
			xj += coord[3*(ipoly*N+NA)];
			xj /= 2;
double			xcmA=0;
			for (unsigned int imono=0;imono<NA;imono++)
			  	xcmA += coord[3*(ipoly*N+imono)];
			xcmA /= (double)NA;
double			xcmB=0;
			for (unsigned int imono=NA;imono<N;imono++)
			  	xcmB += coord[3*(ipoly*N+imono)];
			xcmB /= (double)(N-NA);
			if (xcmB>xcmA)
				for (unsigned int imono=0;imono<N;imono++)
			  		coord[3*(ipoly*N+imono)] = L/4 + xj-coord[3*(ipoly*N+imono)];
			else
				for (unsigned int imono=0;imono<N;imono++)
			  		coord[3*(ipoly*N+imono)] = L/4 + coord[3*(ipoly*N+imono)]-xj;
		}
		R2 += (coord[3*N*ipoly+3*(N-1)]-coord[3*N*ipoly])*(coord[3*N*ipoly+3*(N-1)]-coord[3*N*ipoly])
		     + coord[3*N*ipoly+3*(N-1)+1]                * coord[3*N*ipoly+3*(N-1)+1]
		     + coord[3*N*ipoly+3*(N-1)+2]                * coord[3*N*ipoly+3*(N-1)+2];
	}/* ipoly */
	R2 /= (double)n;
	fprintf(stderr,"INFO: generated new configuration n= %d ; N= %d ; R2= %g ;\n",n,N,R2);
	return(n);
}/* end genconf */

/*----------------------------------------------------------------------------------------------*/
int	writeconf(int *ptime,char *fname)		/* write out configuration */
{
FILE	*fp;
	if (fname==NULL)
	  	fp = fopen("conf.dat","w");
	else
	  	fp = fopen(fname,"w");
	if (fp==NULL) {	/* write out data */
	  	fprintf(stderr,"ERROR: Cannot write conf.dat\n");
		exit(1);
	}
	if (fname==NULL)
		fprintf(fp,"# %d %d\n",n,N);
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
		for (unsigned int imono=0;imono<N;imono++) {
unsigned int		i = 3*(ipoly*N+imono);
		  	if (imono==0)
	  			fprintf(fp,"%g %g %g   %d %d %d\n",xcoord[i],xcoord[i+1],xcoord[i+2],ptime[ipoly+2*n],ptime[ipoly+3*n],ptime[ipoly+4*n]);
			else
			  	fprintf(fp,"%g %g %g\n",xcoord[i],xcoord[i+1],xcoord[i+2]);
		}
	}
	fclose(fp);
	return(n);
}/* end writeconf */

/*----------------------------------------------------------------------------------------------*/
int     readconf(int *ptime)				/* read in configuration */
{
int     genconf(double *coord);
double	*coord = xcoord + 3*n*N;
FILE	*fp=fopen("conf.dat","r");
	if (fp==NULL) {	/* try to read data */
	  	fprintf(stderr,"ERROR: Cannot read conf.dat => generate configuration\n");
		genconf(coord);
		return(n);
	}
unsigned int	n1=0,N1=0,flag=0;
	fscanf(fp,"# %u %u\n",&n1,&N1);
	if ((n1<n)||(N1!=N)) {
	  	fprintf(stderr,"ERROR: n= %d < %d or N= %d != %d\n",n,n1,N,N1);
		fclose(fp);
		exit(1);
	}
unsigned int*	choose=(unsigned int*)calloc(n1,sizeof(unsigned int));
	if (choose==NULL) {
	  	fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(-1);
	}
	for (unsigned int i=0;i<n1;i++)			/* generate a permutation */
	  	choose[i] = i;
	if (n1>n) {
	  	fprintf(stderr,"INFO: %d polymers in conf.dat; choose %d distinct ones at random\n",n1,n);
		for (unsigned int i=0;i<n1/2;i++) {	/* swap entries */
unsigned int		ipoly = int_pcg32()%n1;
unsigned int		jpoly = int_pcg32()%n1;
unsigned int		tmp   = choose[ipoly];
			choose[ipoly] = choose[jpoly];
			choose[jpoly] = tmp;
		}
	}
	for (unsigned int ipoly=0;ipoly<n1;ipoly++) {
unsigned int	ichosen = choose[ipoly];
	  	for (unsigned int imono=0;imono<N;imono++) {
double			xc,yc,zc;
int			ipolyorg,itlast,itime;
			if (imono==0) {
char*				line_buf = NULL;	/* is allocated by getline */
size_t 				line_buf_size = 0;
ssize_t				line_size = getline(&line_buf,&line_buf_size,fp);
				if (line_size<=0) {
				  	fprintf(stderr,"ERROR: (1) Cannot read coords and info @ ipoly= %d imono= %d\n",ipoly,imono);
                        		exit(-1);
				}
int	  			iread = sscanf(line_buf,"%lg %lg %lg   %d %d %d\n",&xc,&yc,&zc,&ipolyorg,&itlast,&itime);
		                if ((iread<3)||(iread>6)) {
				  	fprintf(stderr,"ERROR: (2) Cannot read coords and info iread= %d @ ipoly= %d imono= %d\ninput line: %s",iread,ipoly,imono,line_buf);
                        		exit(-1);
				}
				else if (((iread<6)||(task<2))&&(task!=-1)) {		/* read from label_0.dat / escape */
				  	ipolyorg = -1;
					itlast   = -1;
				  	itime    = 0;
					flag     = 1;	/* set flag to write label_1_used.dat */
				  	if (ichosen<n)
				  		fprintf(stderr,"INFO: set ipoly= %d -> ichosen= %d itime= %d\n",ipoly,ichosen,itime);
				}
                		else if ((iread==6)&&(task!=-1)) {
				  	fprintf(stderr,"INFO: info @ ipoly= %d -- ipolyorg = %d itlast= %d itime= %d\n",ipoly,ipolyorg,itlast,itime);
				}
				else if (task!=-1){
				  	fprintf(stderr,"INFO: readconf -- should never reach this line\n");
					exit(-1);
				}
				free(line_buf);
			}
			else {
int	  			iread = fscanf(fp,"%lg %lg %lg\n",&xc,&yc,&zc);
				if (iread!=3) {
				  	fprintf(stderr,"ERROR: Cannot read coords @ ipoly= %d imono= %d\n",ipoly,imono);
					exit(-1);
				}
			}
			if (ichosen<n) {
				if (imono==0) {
				  	if (flag)
						ptime[ichosen+2*n] = irun*n+ichosen;	/* refers to label_1_used not label_1.dat */
					else
						ptime[ichosen+2*n] = irun*n+ipoly;	/* ipoly refers to conf.dat / label_2*.dat */
					ptime[ichosen+3*n] = itime;
					ptime[ichosen+4*n] = itime;
				}
int				i = 3*(ichosen*N+imono);
				coord[i  ] = xc;
				coord[i+1] = yc;
				coord[i+2] = zc;
			}
		}
	}
	fclose(fp);
	free(choose);
	if ((task==-1)&&(NA!=0)&&(NA!=N)) {
		for (unsigned int ipoly=0;ipoly<n;ipoly++)
	  		breaksym(coord,ipoly);
	}
	if ((flag)&&(task==2)) {
char 		fname[2048];
		sprintf(fname,"label_1_used.dat");
	  	fprintf(stderr,"INFO: write %s\n",fname);
		writeconf(ptime,fname);			/* write conf.dat */
	}
        for (unsigned int i=0;i<3*n*N;i++)
		xcoord[i] = coord[i];              	/* copy starting configuration to configuration */
#ifdef TEST
double	CVmin = +1000;
double	CVmax = -1000;
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
double		CV = calc_CV(ipoly);
	 	if (CV<CVmin)
		  	CVmin = CV;
		if (CV>CVmax)
		  	CVmax = CV;
	}
	fprintf(stderr,"INFO: read %d polymers with %g <= CV <= %g\n",n,CVmin,CVmax);
#endif
	return(n);
}/* end readconf */

/*----------------------------------------------------------------------------------------------*/
int	breaksym(double *coord,const unsigned int ipoly)
{
  	if ((NA==0)||(NA==N)) {
	  	return(0);
	}
static int flag = 1;
	if (flag) {
	  	fprintf(stderr,"INFO: break symmetry upon read (L= %g)\n",L);
		flag = 0;
	}
double	xj  = coord[3*(ipoly*N+NA-1)];
	xj += coord[3*(ipoly*N+NA)];
	xj /= 2;
double	d1  = xj - L/4;
	d1 -= L*rint(d1/L);				/* mic */
double	d2  = xj - 3*L/4;
	d2 -= L*rint(d2/L);
	if (fabs(d2)<fabs(d1)) {			/* shift and flip */
	  	fprintf(stderr,"INFO: shift and flip polymer %u\n",ipoly);
double	  	xp = L*rint((xj-3*L/4)/L);
	  	for (unsigned int imono=0;imono<N;imono++) {
		  	d2  = coord[3*(ipoly*N+imono)] - 3*L/4 - xp;
		  	coord[3*(ipoly*N+imono)] = L/4 - d2;
		}
	  	return(1);
	}
	return(0);
}/* breaksym */

/*----------------------------------------------------------------------------------------------*/
void	ana(int istep,double *x0)			/* analyze configuration */
{
static int	Nana=0;
static double 	*xcm0=NULL,*R0,*X0,*profA,*profB,*profe,*profcm,*profjp,*profcmA,*profcmB;
unsigned int	Nx=96;

/* allocate memory and initialize */
	if (xcm0==NULL) {				/* calc center of mass, R, and X for initial configuration */
	  	if ((xcm0=(double*)calloc(9*n,sizeof(double)))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot allocate memory in ana\n");
			exit(1);
		}
		R0 = xcm0 + 3*n;
		X0 = xcm0 + 6*n;
		for (unsigned int ipoly=0;ipoly<n;ipoly++) {
double			cmx=0,cmy=0,cmz=0,Xx=0,Xy=0,Xz=0;
		  	for (unsigned int imono=0;imono<N;imono++) {
unsigned int		  	index = 3*(N*ipoly+imono);
				cmx  += x0[index  ];
				cmy  += x0[index+1];
				cmz  += x0[index+2];
				Xx   += x0[index  ]*cos(M_PI*(imono+0.5)/(double)N);
				Xy   += x0[index+1]*cos(M_PI*(imono+0.5)/(double)N);
				Xz   += x0[index+2]*cos(M_PI*(imono+0.5)/(double)N);
			}
			xcm0[3*ipoly  ] = cmx/N;
			xcm0[3*ipoly+1] = cmy/N;
			xcm0[3*ipoly+2] = cmz/N;
			R0[3*ipoly  ]   = x0[3*N*ipoly+3*(N-1)  ] - x0[3*N*ipoly  ];
			R0[3*ipoly+1]   = x0[3*N*ipoly+3*(N-1)+1] - x0[3*N*ipoly+1];
			R0[3*ipoly+2]   = x0[3*N*ipoly+3*(N-1)+2] - x0[3*N*ipoly+2];
			X0[3*ipoly  ]   = Xx/N;
			X0[3*ipoly+1]   = Xy/N;
			X0[3*ipoly+2]   = Xz/N;
		}/* ipoly */
	  	if ((profA=(double*)calloc(7*Nx,sizeof(double)))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot allocate memory in ana\n");
			exit(1);
		}
		profB   = profA + Nx;
		profe   = profA + 2*Nx;
		profcm  = profA + 3*Nx;
		profjp  = profA + 4*Nx;
		profcmA = profA + 5*Nx;
		profcmB = profA + 6*Nx;
	}/* initialize */
int	flag;
	if (istep<0) {
	  	istep = -istep;
		flag = 1;
	}

/* analyze */
double	zintra=0,g3=0,g3x=0,g3y=0,g3z=0,rcorr=0,Xcorr=0,R2=0,RA2=0,RB2=0,X2=0;
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {	/* calc g3 and R2 */
double		cmx=0,cmy=0,cmz=0,Xx=0,Xy=0,Xz=0;
	  	for (unsigned int imono=0;imono<N;imono++) {
unsigned int		index   = 3*(N*ipoly+imono);
			cmx    += xcoord[index  ];
			cmy    += xcoord[index+1];
			cmz    += xcoord[index+2];
			Xx     += xcoord[index  ]*cos(M_PI*(imono+0.5)/(double)N);
			Xy     += xcoord[index+1]*cos(M_PI*(imono+0.5)/(double)N);
			Xz     += xcoord[index+2]*cos(M_PI*(imono+0.5)/(double)N);
unsigned int		tmp     = 0;
			calc_Enb_pair(ipoly,imono,&tmp);
			zintra += (double)tmp;
		}
double		d      = cmx/N-xcm0[3*ipoly  ];
		g3x   += d*d;
		g3    += d*d;
		d      = cmy/N-xcm0[3*ipoly+1];
		g3y   += d*d;
		g3    += d*d;
		d      = cmz/N-xcm0[3*ipoly+2];
		g3z   += d*d;
		g3    += d*d;
		d      = xcoord[3*N*ipoly+3*(N-1)  ] - xcoord[3*N*ipoly  ];
		rcorr += d * R0[3*ipoly  ];
		R2    += d*d;
		d      = xcoord[3*N*ipoly+3*(N-1)+1] - xcoord[3*N*ipoly+1];
		rcorr += d * R0[3*ipoly+1];
		R2    += d*d;
		d      = xcoord[3*N*ipoly+3*(N-1)+2] - xcoord[3*N*ipoly+2];
		rcorr += d * R0[3*ipoly+2];
		R2    += d*d;
		if (NA>0) {
			d      = xcoord[3*N*ipoly+3*(NA-1)  ] - xcoord[3*N*ipoly  ];
			RA2   += d*d;
			d      = xcoord[3*N*ipoly+3*(NA-1)+1] - xcoord[3*N*ipoly+1];
			RA2   += d*d;
			d      = xcoord[3*N*ipoly+3*(NA-1)+2] - xcoord[3*N*ipoly+2];
			RA2   += d*d;
		}
		if (NA<N) {
			d      = xcoord[3*N*ipoly+3*(N-1)  ] - xcoord[3*N*ipoly+3*NA  ];
			RB2   += d*d;
			d      = xcoord[3*N*ipoly+3*(N-1)+1] - xcoord[3*N*ipoly+3*NA+1];
			RB2   += d*d;
			d      = xcoord[3*N*ipoly+3*(N-1)+2] - xcoord[3*N*ipoly+3*NA+2];
			RB2   += d*d;
		}
		Xcorr += (Xx/N)*X0[3*ipoly  ];
		Xcorr += (Xy/N)*X0[3*ipoly+1];
		Xcorr += (Xz/N)*X0[3*ipoly+2];
		X2    += (Xx*Xx+Xy*Xy+Xz*Xz)/(double)(N*N);
	}/* ipoly */
	zintra /= n*N;
	g3     /= n;
	g3x    /= n;
	g3y    /= n;
	g3z    /= n;
	rcorr  /= n;
	Xcorr  /= n;
	R2     /= n;
	RA2    /= n;
	RB2    /= n;
	X2     /= n;

double	g1 = 0;
	for (unsigned int index=0;index<3*n*N;index++) {	/* calc g1 in x direction */
double	  	d   = xcoord[index] - x0[index];
		g1 += d*d;
	}
	g1 /= n*N;

	Nana++;
double	dx = L/(double)Nx;
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
double	  	x  = calc_xc(ipoly);
unsigned int	ix = (int)floor(x/dx);
		profcm[ix]++;
	  	x  = calc_xj(ipoly);
		ix = (int)floor(x/dx);
		profjp[ix]++;
	  	x  = calc_xcA(ipoly);
		ix = (int)floor(x/dx);
		profcmA[ix]++;
	  	x  = calc_xcB(ipoly);
		ix = (int)floor(x/dx);
		profcmB[ix]++;
	  	for (unsigned int imono=0;imono<N;imono++) {
unsigned int		index = 3*(ipoly*N+imono);
double		  	xp  = xcoord[index];
			xp -= L*((int)(xp/L));			/* pbc */
			if (xp<0)
			  	xp += L;
			ix  = (int)floor(xp/dx);
			if (imono<NA)
			  	profA[ix]++;
			else
			  	profB[ix]++;
			if ((imono==0)||(imono==N-1))
			  	profe[ix]++;
		}
	}

/* append data file ana.dat */
FILE	*fp;
	if ((fp=fopen("ana.dat","a"))==NULL) {
	  	fprintf(stderr,"ERROR: Cannot append ana.dat\n");
		exit(-1);
	}
	fprintf(fp,"%d   g1= %g   g3= %g %g %g %g  rcorr= %g   R2= %g ( %g %g )   zintra= %g   Xcorr= %g   X2= %g\n",
	    	istep,g1,g3,g3x,g3y,g3z,rcorr,R2,RA2,RB2,zintra,Xcorr,X2);
	fclose(fp);

/* write out profiles */
	if (flag) {
		if ((fp=fopen("prof.dat","w"))==NULL) {		/* write out data */
	  		fprintf(stderr,"ERROR: Cannot append prof.dat\n");
			exit(-1);
		}
double		norm = (double)Nx/(double)(n*Nana);
		for (unsigned int ix=0;ix<Nx;ix++) {
		  	fprintf(fp,"%g   %g %g : %g %g %g   %g %g\n",(ix+0.5)*L/(double)Nx,
			    	profA[ix]*norm/N,profB[ix]*norm/N,
				profe[ix]*norm,profcm[ix]*norm,profjp[ix]*norm,
				profcmA[ix]*norm,profcmB[ix]*norm);
		}
		fclose(fp);
		if ((fp=fopen("prof_sym.dat","w"))==NULL) {	/* write out data */
	  		fprintf(stderr,"ERROR: Cannot append prof.dat\n");
			exit(-1);
		}
		norm = (double)Nx/(double)(2*n*Nana);
		for (unsigned int ix=0;ix<Nx;ix++) {
		  	fprintf(fp,"%g   %g %g : %g %g %g   %g %g\n",(ix+0.5)*L/(double)Nx,
			    	(profA[ix]+profA[Nx-1-ix])*norm/N,(profB[ix]+profB[Nx-1-ix])*norm/N,
				(profe[ix]+profe[Nx-1-ix])*norm,(profcm[ix]+profcm[Nx-1-ix])*norm,(profjp[ix]+profjp[Nx-1-ix])*norm,
				(profcmA[ix]+profcmA[Nx-1-ix])*norm,(profcmB[ix]+profcmB[Nx-1-ix])*norm);
		}
		fclose(fp);
	}
}/* end ana */

/*----------------------------------------------------------------------------------------------*/
void	mc_local(const int istep,int *ptime,unsigned long *try_adr,unsigned long *acc_adr,double *dEacc_adr)	/* local MC move, mimicking local Rouse-like dynamics */
{	/* remark: for the task a one-dimensional simulation would be sufficient because the three Cartesian directions decouple */
double	dx    = 1./sqrt(N-1);
int	Ntry  = 0;
int	Nacc  = 0;
double	dEacc = 0;

#pragma omp parallel for default(shared) reduction(+:dEacc) reduction(+:Nacc) reduction(+:Ntry)
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {		/* loop over all polymers (in parallel) */
double		dEipoly   = 0;
int		Naccipoly = 0;
int		Ntryipoly = 0;
unsigned int	*choose;
  		if ((choose=(unsigned int*)calloc(N,sizeof(unsigned int)))==NULL) {
	  		fprintf(stderr,"ERROR: Cannot allocate memory\n");
			exit(-1);
		}
	  	if (ptime[ipoly]==0) {
			for (unsigned int i=0;i<N;i++) {
		 		choose[i] = i;
			}
			for (unsigned int i=0;i<N/2;i++) {	/* swap entries */
unsigned int			imono = int_pcg32()%N;
unsigned int			jmono = int_pcg32()%N;
int				tmp   = choose[imono];
				choose[imono] = choose[jmono];
				choose[jmono] = tmp;
			}
			ptime[ipoly+4*n]++;
	  		for (unsigned int i=0;i<N;i++) {
	  			if (ptime[ipoly]==0) {
unsigned int			  	imono  = choose[i];
unsigned int				index  = 3*(ipoly*N+imono);
double					xold   = xcoord[index  ];
double					yold   = xcoord[index+1];
double					zold   = xcoord[index+2];
double					xnew   = xold + dx*(2*double_pcg32()-1.);
double					ynew   = yold + dx*(2*double_pcg32()-1.);
double					znew   = zold + dx*(2*double_pcg32()-1.);
double					dEnb   = 0;
					dEnb  -= calc_Enb_extf(ipoly,imono);
unsigned int				zintra = 0;
					dEnb  -= calc_Enb_pair(ipoly,imono,&zintra);
					xcoord[index]   = xnew;
					xcoord[index+1] = ynew;
					xcoord[index+2] = znew;
					dEnb  += calc_Enb_extf(ipoly,imono);
					dEnb  += calc_Enb_pair(ipoly,imono,&zintra);
					xcoord[index]   = xold;
					xcoord[index+1] = yold;
					xcoord[index+2] = zold;
double					dE     = 0;
					if (imono>0) {
double					  	b   = xold-xcoord[index-3];	/* can be improved (n-x)*(n-x)-(o-x)*(o-x) = n*n-2*n*x+x*x-o*o+2*o*x+x*x = n*n-o*o+2*(o-n)*x */
					  	dE -= b*b;
					  	b   = xnew-xcoord[index-3];
					  	dE += b*b;
					  	b   = yold-xcoord[index-2];
					  	dE -= b*b;
					  	b   = ynew-xcoord[index-2];
				  		dE += b*b;
					  	b   = zold-xcoord[index-1];
				  		dE -= b*b;
					  	b   = znew-xcoord[index-1];
					  	dE += b*b;
					}
					if (imono<N-1) {
double					  	b   = xold-xcoord[index+3];
					  	dE -= b*b;
					  	b   = xnew-xcoord[index+3];
				  		dE += b*b;
					  	b   = yold-xcoord[index+4];
					  	dE -= b*b;
					  	b   = ynew-xcoord[index+4];
					  	dE += b*b;
					  	b   = zold-xcoord[index+5];
				  		dE -= b*b;
					  	b   = znew-xcoord[index+5];
					  	dE += b*b;
					}
					dE  *= 3.*(N-1)/2.;
					dE  += dEnb;
					if (dE<0||double_pcg32()<exp(-dE)) {
						xcoord[index  ]   = xnew;
						xcoord[index+1]   = ynew;
						xcoord[index+2]   = znew;
						dEipoly   += dE;
						Naccipoly ++;
int						mnew           = dump(ipoly,istep,ptime); /* check after every accepted move */
						ptime[ipoly+n] = mnew;
					}
					Ntryipoly ++;
				}
			}/* i */
		}
		Ntry  += Ntryipoly;
		Nacc  += Naccipoly;
		dEacc += dEipoly;
		free(choose);
	}/* ipoly */
	(*try_adr)   += Ntry;
  	(*acc_adr)   += Nacc;
	(*dEacc_adr) += dEacc;
}/* end mc_local */

/*----------------------------------------------------------------------------------------------*/
int	dump(const int ipoly,const int istep,int *ptime) /* write down configurations at FF interface to disk */
{
/*	task 	action
 *	-1	do nothing
 *		if ipoly<0 : do nothing if no open file (MC simulation) or close file, write "time.dat" and reset
 *	0	dump confs in basin	dump if last dump is longer than embargo ago
 *	1	escape from basin	freeze at m==1	between basin and FF interface
 *	2	reach FF interface	freeze at m==0 (basin) or m==2 FF interface
 */

#define FOPEN\
  	{\
char		fname[2048];\
		sprintf(fname,"label_%d.dat",task);\
		if ((fp=fopen(fname,"a"))==NULL) {\
	  		fprintf(stderr,"ERROR: Cannot append %s\n",fname);\
			exit(-1);\
		}\
	}

#define FCLOSE\
  	{\
	  	fclose(fp);\
	}

FILE	*fp;
static int	count=0;
static int	save=0;
static int	*last=NULL;
static double	CVmax = -1000;
static double 	CVmin = +1000;

	if (task<0)
	  	return(-1);				/* not a valid m score */
	if (ipoly<0) {					/* not a valid polymer index: close file on last call and append time.dat */
		if ((fp=fopen("time.dat","a"))==NULL) {
	  		fprintf(stderr,"ERROR: Cannot append time.dat\n");
			exit(-1);
		}
		for (unsigned int ipoly=0;ipoly<n;ipoly++) {
		  	fprintf(fp,"%d   %d   %d   CV= %g\n",ipoly,calc_m(ipoly),ptime[ipoly],calc_CV(ipoly));
		}
		fclose(fp);
		if (last!=NULL)
			free(last);
		last = NULL;
		return(-1);				/* not a valid m score */
	}
int	embargo = 2000;					/* time steps between configurations saved in starting basin */
	if (last==NULL) {				/* allocate memory for time, at which polymer has been saved the last time */
	  	if ((last=(int*)calloc(n,sizeof(int)))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot allocate memory\n");
			exit(-1);
		}
		for (unsigned int ipoly=0;ipoly<n;ipoly++)
		  	last[ipoly] = -embargo-1;
	}
	if (ptime[ipoly]==0) {				/* not froozen */
int	 	m = calc_m(ipoly);			/* calculate score of polymer, ipoly, wrt to FF interface */
		switch(task) {
		  	case 0:				/* dump configurations in basin (up to CV_border) */
				if ((m<2)&&(istep-last[ipoly]>embargo)) {	/* not in opposite basin && time diff larger than embargo) */
				  	save++;
					fprintf(stderr,"INFO: (%d) save %d th polymer %d at step %d (last= %d) with m= %d @ CV= %g\n",
				    		task,save,ipoly,istep,last[ipoly],m,calc_CV(ipoly));
					FOPEN;
		  			for (unsigned int imono=0;imono<N;imono++) {
unsigned int		  			i = 3*(ipoly*N+imono);
		  	 			fprintf(fp,"%g %g %g\n",xcoord[i],xcoord[i+1],xcoord[i+2]);
					}
					FCLOSE;
					last[ipoly] = istep;
				}
			  	break;
			case 1:				/* escape from basin */
				if ((ptime[ipoly+n]==0)&&(m==1)) {		/* jump from basin across 0th interface (no freeze) */
double					CV = calc_CV(ipoly);
					if (CV<CVmin)
			  			CVmin = CV;
					if (CV>CVmax)
			  			CVmax = CV;
					count++;
					fprintf(stderr,"INFO: (%d) count= %d polymer %d at step %d with m= %d @ CV= %g [ %g : %g ]\n",
			    			task,count,ipoly,istep,m,CV,CVmin,CVmax);
					if (count%2==1) {	/* only save every 2th */
					  	save++;
						fprintf(stderr,"INFO: (%d) save %d th polymer %d at step %d (last= %d) with m= %d @ CV= %g\n",
			    				task,save,ipoly,istep,last[ipoly],m,CV);
						FOPEN;
		  				for (unsigned int imono=0;imono<N;imono++) {
unsigned int			  			i = 3*(ipoly*N+imono);
		  	 				fprintf(fp,"%g %g %g\n",xcoord[i],xcoord[i+1],xcoord[i+2]);
						}
						FCLOSE;
						last[ipoly] = istep;
					}
				}
				if (m==2) {				/* reached opposite basin; reinitialize */
double					CV    = calc_CV(ipoly);		/* updated configuration to be replaced */
unsigned int				itest = ipoly;
				  	while(1) {
						itest = int_pcg32()%n;
						if (ptime[itest+n]<2) {
		  					for (unsigned int imono=0;imono<N;imono++) {
unsigned int		  					i = 3*(ipoly*N+imono);
unsigned int		  					j = 3*(itest*N+imono);
								xcoord[i]   = xcoord[j];
								xcoord[i+1] = xcoord[j+1];
								xcoord[i+2] = xcoord[j+2];
							}
						}
						ptime[ipoly+n] = ptime[itest+n];
						break;
					}
					fprintf(stderr,"INFO: (%d) polymer %d reached m= %d at step %d CV= %g -> replace by duplicate of polymer %d with m= %d (%d) CV= %g\n",
					    	task,ipoly,m,istep,CV,itest,ptime[ipoly+n],calc_m(ipoly),calc_CV(ipoly));
				}
				break;
			case 2:					/* reach interface */
		  		if (m==0) {
			  		ptime[ipoly] = -istep;	/* freeze */
#ifdef TEST
					fprintf(stderr,"INFO: (%d) freeze polymer %d at step %d with m= %d ptime= %d @ CV= %g\n",
					    	task,ipoly,istep,m,ptime[ipoly],calc_CV(ipoly));
#endif
				}
				if (m==2) {
			  		ptime[ipoly] =  istep;	/* freeze and dump */
#ifdef TEST
					fprintf(stderr,"INFO: (%d) freeze polymer %d at step %d with m= %d ptime= %d @ CV= %g\n",
				    		task,ipoly,istep,m,ptime[ipoly],calc_CV(ipoly));
#endif
double					CV = calc_CV(ipoly);
					if (CV<CVmin)
			  			CVmin = CV;
					if (CV>CVmax)
			  			CVmax = CV;
					fprintf(stderr,"INFO: (%d) dump polymer %d [iCV= %d irun= %d] at step %d with m= %d @ CV= %g [ %g : %g ]\n",
			    			task,ipoly,iCV,irun,istep,m,CV,CVmin,CVmax);
					FOPEN;
		  			for (unsigned int imono=0;imono<N;imono++) {
unsigned int		  			i = 3*(ipoly*N+imono);
						if (imono==0) {
		  	 				fprintf(fp,"%g %g %g   %d %d %d\n",
							    	xcoord[i],xcoord[i+1],xcoord[i+2],ptime[ipoly+2*n],ptime[ipoly+3*n],ptime[ipoly+4*n]);
						}
						else {
		  	 				fprintf(fp,"%g %g %g\n",xcoord[i],xcoord[i+1],xcoord[i+2]);
						}
					}
					FCLOSE;
					ptime[ipoly+4*n] *= (-1);	/* flag to save in order to save polymers just frozen */
				}
				break;
		}
		return(m);
	}/* ptime initially = 0 */
	else {
	  	return(-1);					/* not a valid m score */
	}
#undef FOPEN
#undef FCLOSE
}/* end dump */

/*----------------------------------------------------------------------------------------------*/
void 	save(int *ptime,int dt)
{
  	if (task!=2)
	  	return;
char	fname[2048];
	sprintf(fname,"scoord.dat");
FILE*   fp = fopen(fname,"a");
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
	  	if ((ptime[ipoly]==0)||(ptime[ipoly+4*n]<0)) {
		  	if (ptime[ipoly+4*n]<0)
			  	ptime[ipoly+4*n] *= (-1);
		  	if (ptime[ipoly+4*n]%dt==0) {
			  	fprintf(fp,"%12d %6d %6d ( %1.7f ): ",
				    	ptime[ipoly+2*n],ptime[ipoly+3*n],ptime[ipoly+4*n],calc_CV(ipoly));
				for (unsigned int imono=0;imono<N;imono++) {
				  	fprintf(fp,"%g ",xcoord[3*(ipoly*N+imono)]);
				}
				fprintf(fp,"\n");
			}
		} /* active */
	}
	fclose(fp);
}/* save */

/*----------------------------------------------------------------------------------------------*/
int	calc_active(int *ptime)
{
int	active = 0;
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {
		if (ptime[ipoly]==0) { 				/* count polymers that are still active */
		  	active++;
#ifdef TEST
int		  	m = calc_m(ipoly);			/* calculate score of polymer, ipoly, wrt to FF interface */
			if ((task==1)&&(m==2)) {
				fprintf(stderr,"ERROR: task=1 and m= %d (should be 0 or 1) for an active polymer\n",m);
				exit(-1);
			}
			if ((task==2)&&(m!=1)) {
				fprintf(stderr,"ERROR: task=2 and m= %d (should be 1) for an active polymer\n",m);
				exit(-1);
			}
#endif
		}
	}/* ipoly */
	return(active);
}/* end calc_active */

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
