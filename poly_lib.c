#define TEST
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
#include "poly_lib.h"
#ifndef M_PI
#define M_PI           3.14159265358979323846  /* pi */
#endif

/*----------------------------------------------------------------------------------------------*/
/* global variables for this file */
static double 	dx,*wA=NULL,*wB,*gA,*gB;	/* used in poly_lib.c for functions calc_Enb calc_Eself */
static int	*type=NULL,Nx;

/*----------------------------------------------------------------------------------------------*/
double	calc_E(void)	/* calculate energy, bonded and nonbonded */
{
double	E=0;
	for (unsigned int ipoly=0;ipoly<n;ipoly++) {	/* bonded energy */
	  	for (unsigned int imono=0;imono<N-1;imono++) {
unsigned int		index1 = 3*(N*ipoly+imono);
unsigned int		index2 = index1+3;
double		  	b  = xcoord[index2  ] - xcoord[index1  ];
			E += b*b;
		  	b  = xcoord[index2+1] - xcoord[index1+1];
			E += b*b;
		  	b  = xcoord[index2+2] - xcoord[index1+2];
			E += b*b;
		}
	}/* ipoly */
	E  *= 3.*(N-1)/2.;
	E  -= 1.5*n*(N-1);
double 	Enb_extf=0,Enb_pair=0;
	for (unsigned int ipoly=0;ipoly<n;ipoly++)
	  	for (unsigned int imono=0;imono<N;imono++) {
		  	Enb_extf += calc_Enb_extf(ipoly,imono);
unsigned int		zintra = 0;
			Enb_pair += 0.5*calc_Enb_pair(ipoly,imono,&zintra);
		}
	fprintf(stderr,"INFO: E = %g (b) %g (extf) %g (pair)\n",E,Enb_extf,Enb_pair);
	return(E+Enb_extf+Enb_pair);
}/* end calc_E */

/*----------------------------------------------------------------------------------------------*/
double	calc_Enb_extf(unsigned int ipoly,unsigned int imono)	/* calculate nonbonded energy of a bead */
{
/* initialze: read in omega fields and obtain system size */
	if (wA==NULL) {
		if ((type=(int*)calloc(N,sizeof(int)))==NULL) {
	  		fprintf(stderr,"ERROR: Cannot allocate memory for type\n");
			exit(-1);
		}
		if (N%2!=0) {
		  	fprintf(stderr,"ERROR: init diblock but N mod 2= %d !=0\n",N%2);
			exit(-1);
		}
		for (unsigned int i=0;i<NA;i++)
		  	type[i] = -1;
		for (unsigned int i=NA;i<N;i++)
		  	type[i] = +1;
FILE*		fp = fopen("omega.dat","r");
	  	if (fp==NULL) {
		  	fprintf(stderr,"WARNING: Cannot open omega.dat -> use default setting\n");
			if ((NA==0)||(NA==N))	/* homopolymer */
				L = 10;
			else
			  	L = 10;		/* diblock at interface */
			Nx = 100*(int)floor(L);
			Nx = 4*(int)floor(Nx/4);
			fprintf(stderr,"INFO: default initialization with Nx= %d   L= %g\n",Nx,L);
		}
		else {
int		  	iread = fscanf(fp,"# L= %lg ; Nx= %d ;\n",&L,&Nx);
			if (iread!=2) {
			  	fprintf(stderr,"ERROR: Cannot parse 1st line of omega.dat\n");
				exit(-1);
			}
			fprintf(stderr,"INFO: read omega.dat with Nx= %d   L= %g\n",Nx,L);
		}
		dx = L/(double)Nx;
		if ((wA=(double*)calloc(4*Nx,sizeof(double)))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot allocate memory for wA and wB\n");
			exit(-1);
		}
		wB = wA +   Nx;
		gA = wA + 2*Nx;
		gB = wA + 3*Nx;
		if (fp==NULL) {
double			chiN = 20;
			if (invzc>1e-6)
			  	chiN = 2*eps*N/invzc;
			fprintf(stderr,"INFO: chiN= %g\n",chiN);
		  	if (NA==0) { 		/* B-homopolymer in B phase */
				for (unsigned int ix=0;ix<Nx;ix++) {
					wA[ix] =  0.5*chiN/(double)N;
					wB[ix] = -0.5*chiN/(double)N;
				}
			}
			else if (NA==N) {	/* A-homopolymer in B phase */
				for (unsigned int ix=0;ix<Nx;ix++) {
					wA[ix] =  0.5*chiN/(double)N;
					wB[ix] = -0.5*chiN/(double)N;
				}
			}
			else {			/* lamellar phase */
				for (unsigned int ix=0;ix<Nx/4;ix++) {
					wA[ix] =  0.5*chiN/(double)N;
					wB[ix] = -0.5*chiN/(double)N;
				}
				for (unsigned int ix=Nx/4;ix<3*Nx/4;ix++) {
					wA[ix] = -0.5*chiN/(double)N;
					wB[ix] =  0.5*chiN/(double)N;
				}
				for (unsigned int ix=3*Nx/4;ix<Nx;ix++) {
					wA[ix] =  0.5*chiN/(double)N;
					wB[ix] = -0.5*chiN/(double)N;
				}
			}
		}
		else {
			for (unsigned int ix=0;ix<Nx;ix++) {
int				iread=fscanf(fp,"%lg %lg  %*g %*g\n",&(wA[ix]),&(wB[ix]));
				if (iread!=2) {
				  	fprintf(stderr,"ERROR: Cannot read omega.dat at field line %d\n",ix);
					exit(-1);
				}
				wA[ix] /= (double)N;
				wB[ix] /= (double)N;
			}
			fclose(fp);
		}
		for (unsigned int ix=0;ix<Nx-1;ix++) {
		  	gA[ix] = (wA[ix+1]-wA[ix])/dx;
		  	gB[ix] = (wB[ix+1]-wB[ix])/dx;
		}
		gA[Nx-1] = (wA[0]-wA[Nx-1])/dx;
		gB[Nx-1] = (wB[0]-wB[Nx-1])/dx;
#ifdef TEST
		if ((fp=fopen("t1.dat","w"))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot open omega.dat\n");
			exit(-1);
		}
		for (unsigned int ix=0;ix<Nx;ix++)
		  	fprintf(fp,"%g %g %g\n",ix*dx,wA[ix],wB[ix]);
		fclose(fp);
double		xsave1 = xcoord[0];
double		xsave2 = xcoord[3*(N-1)];
		if ((fp=fopen("t2.dat","w"))==NULL) {
		  	fprintf(stderr,"ERROR: Cannot open omega.dat\n");
			exit(-1);
		}
		for (double xp=0;xp<L;xp+=0.1*dx) {
		  	xcoord[0]       = xp;
		  	xcoord[3*(N-1)] = xp;
		  	fprintf(fp,"%g %g %g\n",xp,calc_Enb_extf(0,0),calc_Enb_extf(0,N-1));
		}
		fclose(fp);
		xcoord[0]       = xsave1;
		xcoord[3*(N-1)] = xsave2;
#endif
	}/* initialization */

/* return system size and calculate nonbonded energy */
double	*w,*g;
	if (type[imono]<0) { 		/* set field and gradient according to type */
	  	w = wA;
		g = gA;
	}
	else {
	  	w = wB;
		g = gB;
	}
double	x  = xcoord[3*(N*ipoly+imono)];
double	xp = x - L*((int)(x/L));	/* pbc */
	if (xp<0)
	  	xp += L;
unsigned int ix = (int)floor(xp/dx);
double	delta = xp - ix*dx;
double	Enb   = w[ix] + g[ix]*delta;
	return(Enb);
}/* end calc_Enb_extf */

/*----------------------------------------------------------------------------------------------*/
double	calc_Enb_pair(unsigned int ipoly,unsigned int imono,unsigned int *zintra_adr) 	/* calculate self-energy */
{
const double r2  = 1.0/(N-1.0);	/* square of range of self-interaction */
unsigned int iindex = 3*(N*ipoly+imono);
double	xi     = xcoord[iindex];
double	yi     = xcoord[iindex+1];
double	zi     = xcoord[iindex+2];
int	typei  = type[imono];
double	Enbi   = calc_Enb_extf(ipoly,imono)*invzc;
double	dEnb   = 0;
unsigned int zintra = 0;
/*do not when parallel over chains #pragma omp parallel for default(shared) reduction(+:zintra) reduction(+:dEnb)*/
	for (unsigned int jmono=0;jmono<N;jmono++) {	/* monomer has self-contact */
	  	if (jmono!=imono) {
unsigned int		jindex = 3*(N*ipoly+jmono);
double			dx = xi - xcoord[jindex  ];
double			dy = yi - xcoord[jindex+1];
double			dz = zi - xcoord[jindex+2];
double			d2 = dx*dx + dy*dy + dz*dz;
			if (d2<r2) {	/* intrachain contact */
		  		zintra ++;
				dEnb   -= 0.5*(Enbi+calc_Enb_extf(ipoly,jmono)*invzc);  /* remove (corresponding) interaction with external field */
				dEnb   += -eps*typei*type[jmono];
			}
		}
	}
	*zintra_adr = zintra;
	return(dEnb);
}/* end calc_Enb_pair */

/*----------------------------------------------------------------------------------------------*/
int	calc_m(unsigned int ipoly)	/* calculate score of polymer, ipoly, wrt to FF interface */
{
static double 	flag=1;
static double	CV_FF_save;

/* initialize FF interface */
	if (flag) {
		if (fabs(CV_FF)<1e-6) {
		  	fprintf(stderr,"WARNING: CV_start= %g   CV_FF= %g   CV_end= %g: set CV_FF= CV_border\n",CV_start,CV_FF,CV_end);
			CV_FF = CV_border;
		}
#ifdef CHECKM
		else if (CV_FF > CV_end) {
		  	fprintf(stderr,"WARNING: CV_start= %g   CV_FF= %g   CV_end= %g: set CV_FF= CV_end\n",CV_start,CV_FF,CV_end);
			CV_FF = CV_end;
		}
#else
		else if (CV_FF > CV_end) {
		  	fprintf(stderr,"WARNING: CV_start= %g   CV_FF= %g   CV_end= %g: CV_FF is larger than CV_end\n",CV_start,CV_FF,CV_end);
FILE*                   fp = fopen("done.dat","w");
                        if (fp==NULL) {
                                fprintf(stderr,"ERROR: Cannot open CV.dat\n");
                                exit(-1);
                        }
                        fprintf(fp,"%g\n",CV_FF);
                        fclose(fp);
		}
#endif
	  	fprintf(stderr,"INFO: initialize mscore with: CV_start= %g   CV_FF= %g   CV_end= %g (reverse= %d)\n",CV_start,CV_FF,CV_end,reverse);
		CV_FF_save = CV_FF;
		flag = 0;
	}/* initialization */
	if (fabs(CV_FF_save-CV_FF)>1e-9) {
	  	fprintf(stderr,"WARNING: CV_FF has been changed since initializing calc_m: CV_FF= %g <- %g\n",CV_FF,CV_FF_save);
		CV_FF_save = CV_FF;
	}

double	CV = calc_CV(ipoly);	/* calculate collective coordinate */
	if (CV<CV_start)	/* fall back to starting basin */
	  	return(0);
	else if (CV<CV_FF)	/* active -- polymer is between starting basin and FF interface */
	  	return(1);
	else			/* polymer has reached FF interface */
	  	return(2);
}/* end calc_m */

/*----------------------------------------------------------------------------------------------*/
double  calc_xj(unsigned int ipoly)	/* calculate JP position */
{
	if ((NA==0)||(NA==N)) {
	  	return(0.);
	}
unsigned int	i   = ipoly*N + NA-1;
double	xj  = xcoord[3*i];
	xj += xcoord[3*i+3];
	xj /= 2.;
 	xj -= L*((int)(xj/L));	/* pbc */
	if (xj<0)
		xj += L;
	return(xj);
}/* end calc_xj */

/*----------------------------------------------------------------------------------------------*/
double  calc_xc(unsigned int ipoly)	/* calculate CM position */
{
double	xp = 0;
	for (unsigned int imono=0;imono<N;imono++) {
unsigned int	i   = ipoly*N + imono;
	  	xp += xcoord[3*i];
	}
	xp /= (double)N; 	/* location of center of mass */
 	xp -= L*((int)(xp/L));	/* pbc */
	if (xp<0)
		xp += L;
	return(xp);
}/* end calc_xc */

/*----------------------------------------------------------------------------------------------*/
double  calc_xcA(unsigned int ipoly)	/* calculate CM position */
{
double	xp = 0;
	for (unsigned int imono=0;imono<NA;imono++) {
unsigned int	i   = ipoly*N + imono;
	  	xp += xcoord[3*i];
	}
	if (NA>0)
		xp /= (double)NA;
 	xp -= L*((int)(xp/L));	/* pbc */
	if (xp<0)
		xp += L;
	return(xp);
}/* end calc_xcA */

/*----------------------------------------------------------------------------------------------*/
double  calc_xcB(unsigned int ipoly)	/* calculate CM position */
{
double	xp = 0;
	for (unsigned int imono=NA;imono<N;imono++) {
unsigned int	i   = ipoly*N + imono;
	  	xp += xcoord[3*i];
	}
	if (N-NA>0)
		xp /= (double)(N-NA);
 	xp -= L*((int)(xp/L));	/* pbc */
	if (xp<0)
		xp += L;
	return(xp);
}/* end calc_xcB */

/*----------------------------------------------------------------------------------------------*/
double  calc_X2(unsigned int ipoly)	/* calculate first Rouse mode */
{
double	X2 = 0;
	for (unsigned int imono=0;imono<N;imono++) {
unsigned int	i   = ipoly*N + imono;
		X2 += xcoord[3*i]*cos(M_PI*(imono+0.5)/(double)N);
	}
	X2 /= (double)N;
	X2 *= X2;
	if (reverse)
	  	X2 = -X2;
	return(X2);
}/* end calc_X2 */

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
