/*----------------------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <omp.h>
#include "pcg.h"

#define CHECKM
/*----------------------------------------------------------------------------------------------*/
/* global variables in poly.c (see globals.c) */
extern double		eps;
extern double		invzc;
extern double		L;
extern double		*xcoord;
extern unsigned int    	n;
extern unsigned int    	N;
extern unsigned int	NA;

extern int      	reverse;      /* reverse=0: reverse=1: */
extern double   	CV_FF;
extern int      	task;
extern int		iCV;
extern int		irun;

extern double   	CV_border;
extern double   	CV_start;
extern double   	CV_end;
extern double   	CVR_start;
extern double   	CVR_end;

extern double  (*calc_CV)(unsigned int ipoly);

/*----------------------------------------------------------------------------------------------*/
double	calc_E(void);
double	calc_Enb_extf(unsigned int ipoly,unsigned int imono);
double  calc_Enb_pair(unsigned int ipoly,unsigned int imono,unsigned int *zintra_adr);
int	calc_m(unsigned int ipoly);
double  calc_xj(unsigned int ipoly);
double	calc_xc(unsigned int ipoly);
double	calc_xcA(unsigned int ipoly);
double	calc_xcB(unsigned int ipoly);
double	calc_X2(unsigned int ipoly);

/*----------------------------------------------------------------------------------------------*/
