#define TEST
/*----------------------------------------------------------------------------------------------*/
/*
 * mpicc -lm -O3 -o M -Wall -fopenmp map.c  global.c poly_lib.c
 *
 * usage M n N ip0 NL dt
 *
 */
/*----------------------------------------------------------------------------------------------*/
#include "poly_lib.h"

/*----------------------------------------------------------------------------------------------*/
int	main(int argc,char** argv)
{
	if (argc!=6) {
	  	fprintf(stderr,"ERROR: usage M n N ip0 NL dt\n");
		exit(-1);
	}
/* read in parameters from command line */
int     i = 1;
const int	n   = atoi(argv[i++]);
const int     	N   = atoi(argv[i++]);
const int	ipi = atoi(argv[i++]);
const int	NL  = atoi(argv[i++]);
const int	dt  = atoi(argv[i++]);

int	ip = ipi;
	if ((int)ip/(int)n!=0) {
	  	fprintf(stderr,"ERROR: ip= %d in the final label %d must be in [0:%d[\n",ip,NL,n);
		exit(-1);
	}

double*	x = (double*)calloc(3*N,sizeof(double));
	if (x==NULL) {
	  	fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(-1);
	}

	fprintf(stderr,"INFO: backtrace %d th polymer in label_2_%d.dat\n",ip,NL);
char	rname[2048];
	sprintf(rname,"result_poly=%d.dat",ip);
FILE*	rp = fopen(rname,"w");
	if (rp==NULL) {
	  	fprintf(stderr,"ERROR: Cannot open %s\n",rname);
		exit(-1);
	}
	for (int ilabel=NL;ilabel>=0;ilabel--) {
char		fname[2048];
		sprintf(fname,"label_2_%d.dat",ilabel);
FILE*		fp = fopen(fname,"r");
		if (fp==NULL) {
		  	fprintf(stderr,"ERROR: Cannot open %s\n",fname);
			exit(-1);
		}
int		flag = 1;
double		xc,yc,zc;
int		iorg,itime,itstart;
		for (int ipoly=0;(ipoly<n)&&(flag);ipoly++) {
int			iread = fscanf(fp,"%lg %lg %lg   %d %d %d\n",&xc,&yc,&zc,&iorg,&itstart,&itime); /* iorg = irun*n+ipoly */
			if (iread!=6) {
			  	fprintf(stderr,"ERROR: Cannot read coord and info -- ipoly= %d in file %s\n",
				    	ipoly,fname);
				exit(-1);
			}
			x[0] = xc;
			x[1] = yc;
			x[2] = zc;
			if (ipoly==ip%n)
				flag = 0;
		  	for (int imono=1;imono<N;imono++) {
int			  	iread = fscanf(fp,"%lg %lg %lg\n",&xc,&yc,&zc);
				if (iread!=3) {
				  	fprintf(stderr,"ERROR: Cannot read coord -- ipoly= %d imono= %d in file %s\n",
					    	ipoly,imono,fname);
					exit(-1);
				}
				x[3*imono  ] = xc;
				x[3*imono+1] = yc;
				x[3*imono+2] = zc;
			}
			if (!flag) {
				fprintf(stderr,"INFO: ilabel= %4d %s   %12d [%6d %6d]    %6d %6d    ( %1.7f ) -> %12d [%6d %6d]\n",
				    	ilabel,fname,ip,ip%n,(int)ip/(int)n,itstart,itime,calc_X2(0,N,x),iorg,iorg%n,(int)iorg/(int)n);
int				imax = (int)itime/(int)dt;
int				imin = (int)itstart/(int)dt+1;
				if (ilabel==NL) {
					fprintf(rp,"time= %d ipoly= %d X2= %g : ",itime,ipi,calc_X2(0,N,x));
					for (int imono=0;imono<N;imono++)
					  	fprintf(rp,"%g ",x[3*imono]);
					fprintf(rp,"\n");
				}
				if (imin<=imax) {
char					dname[2048];
					sprintf(dname,"scoord_2_%d.dat",ilabel);
/*					fprintf(stderr,"INFO: try to find ipoly= %d in %s -- [%d %d]\n",ip,dname,imin*dt,imax*dt);*/
FILE*					dp = fopen(dname,"r");
					if (dp==NULL) {
					  	fprintf(stderr,"ERROR; Cannot open %s\n",dname);
						exit(-1);
					}
int					linenr = 0;
int					nmatch = 0;
					while (!feof(dp)) {
					  	linenr++;
int						spoly,stime;
int						iread = fscanf(dp,"%d %*d %d ( %*lf ): ",&spoly,&stime);
						if (iread!=2) {
                                       			fprintf(stderr,"ERROR: iread= %d -- Cannot read line in %s\n",iread,dname);
                                       			exit(-1);
						}
						for (int imono=0;imono<N;imono++) {
						  	fscanf(dp,"%lg ",&(x[3*imono]));
						}
						fscanf(dp,"\n");
						if ((spoly==iorg)&&(stime%dt==0)) {
							for (int i=imin;i<=imax;i++) {
							  	if (stime==i*dt) {
								  	fprintf(stderr,"INFO: file= %s [%6d %6d] -- %12d %6d\n",dname,imin*dt,imax*dt,spoly,stime);
									nmatch++;
									fprintf(rp,"time= %d ipoly= %d X2= %g : ",stime,ipi,calc_X2(0,N,x));
									for (int imono=0;imono<N;imono++)
									  	fprintf(rp,"%g ",x[3*imono]);
									fprintf(rp,"\n");
								}
							}
						}
					}
					fclose(dp);
					if (nmatch!=imax-imin+1) {
					  	fprintf(stderr,"ERROR: Cannot match nmatch= %d imax= %d imin= %d ip= %d [%d %d]\n",
						    	nmatch, imax,imin,ip,imin*dt,imax*dt);
						exit(-1);
					}
				}
				ip     = iorg;
			}
		}
		fclose(fp);
		if (flag) {
		  	fprintf(stderr,"ERROR: Cannot find match in %s\n",fname);
			exit(-1);
		}
	}/* ilabel */
FILE*	fp = fopen("label_1_used.dat","r");
	if (fp==NULL) {
	  	fprintf(stderr,"ERROR; Cannot open label_1_used.dat\n");
		exit(-1);
	}
double	xc,yc,zc;
int	iorg,itime,itstart;
	for (int ipoly=0;ipoly<=(ip%n);ipoly++) {
int		iread = fscanf(fp,"%lg %lg %lg   %d %d %d\n",&xc,&yc,&zc,&iorg,&itstart,&itime);
		if (iread!=6) {
		  	fprintf(stderr,"ERROR: Cannot read coord and info -- ipoly= %d in file label_1_used\n",ipoly);
			exit(-1);
		}
		x[0] = xc;
		x[1] = yc;
		x[2] = zc;
		if ((iorg!=ipoly)||(itstart!=0)||(itime!=0)) {
		  	fprintf(stderr,"ERROR: label_1_used.dat ipoly= %d -- %d %d %d\n",ipoly,iorg,itstart,itime);
			exit(-1);
		}
	  	for (int imono=1;imono<N;imono++) {
int		  	iread = fscanf(fp,"%lg %lg %lg\n",&xc,&yc,&zc);
			if (iread!=3) {
			  	fprintf(stderr,"ERROR: Cannot read coord -- ipoly= %d imono= %d in file label_1_used.dat\n",ipoly,imono);
				exit(-1);
			}
			x[3*imono  ] = xc;
			x[3*imono+1] = yc;
			x[3*imono+2] = zc;
		}
	}
	fprintf(stderr,"INFO: ilabel= -1 label_1_used.dat  %12d [%6d %6d]    %6d %6d    ( %1.7f ) -> %12d [%6d %6d]\n",
    		ip,ip%n,(int)ip/(int)n,itstart,itime,calc_X2(0,N,x),iorg,iorg%n,(int)iorg/(int)n);
	fprintf(rp,"time= %d ipoly= %d X2= %g : ",itime,ipi,calc_X2(0,N,x));
	for (int imono=0;imono<N;imono++)
	  	fprintf(rp,"%g ",x[3*imono]);
	fprintf(rp,"\n");
	fclose(fp);
	fclose(rp);
	free(x);
	return(0);
}/* end main */
