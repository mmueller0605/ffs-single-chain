#ifndef PCG_RANDOM_H
#define PCG_RANDOM_H
#pragma once

/*----------------------------------------------------------------------------------------------*/
void		init_pcg32(void);
void		end_pcg32(void);
unsigned int    int_pcg32(void);
double		double_pcg32(void);
double 		gauss_pcg32(void);

/*----------------------------------------------------------------------------------------------*/
#endif	/* PCG_RANDOM_H */
